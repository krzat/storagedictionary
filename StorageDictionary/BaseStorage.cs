﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ProtoBuf;

namespace StorageLib
{
    /// <summary>
    /// Manages sectors on the file. Encapsulates simple filesystem
    /// </summary>
    [ProtoContract]
    internal class BaseStorage : IDisposable
    {
        [ProtoMember(2)] public readonly int SectorSize;
        private Header _header;

        private FileStream file;
        [ProtoMember(3)] private SortedSet<int> available = new SortedSet<int>();
        [ProtoMember(1)] private List<int> next = new List<int>();

        private StorageStream stream;

        protected BaseStorage()
        {
        }

        public BaseStorage(string path, int sectorSize = 5000)
        {
            this.DataPath = path;
            SectorSize = sectorSize;
        }

        public string DataPath { get; private set; }

        public void Dispose()
        {
            file.Dispose();
        }

        public void Initialize()
        {
            file = File.Open(DataPath, FileMode.OpenOrCreate);
            stream = new StorageStream(this, file);
            Debug.Assert(SectorSize > 0);
        }

        public Stream Open(Header header)
        {
            if (header.Sector == -1)
            {
                header.Sector = AddSector();
            }
            _header = header;
            stream.Open(header);
            return stream;
        }

        public void Remove(Header header)
        {
            SetLength(header, 0);
        }

        public void Truncate()
        {
            var removed = 0;
            while (available.Count > 0)
            {
                var last = available.Last();
                if (last != next.Count - 1 - removed) break;
                removed++;
                available.Remove(last);
            }
            if (removed > 0)
            {
                next.RemoveRange(next.Count - removed, removed);
                var len = (next.Count + 100)*SectorSize;
                if (file.Length > len)
                    file.SetLength(len);
            }
        }

        public void SetLength(Header header, int length)
        {
            var sectors = length/SectorSize;
            if (length%SectorSize != 0) sectors++;
            if (sectors > 0 && header.Sector == -1)
            {
                header.Sector = AddSector();
            }

            var sector = header.Sector;
            var prev = sector;
            var index = 0;
            while (true)
            {
                if (index >= sectors)
                    available.Add(sector);

                index++;

                prev = sector;
                sector = next[sector];
                if (sector == 0)
                {
                    if (index < sectors) sector = AddSector(prev);
                    else break;
                }
            }
            if (length == 0) header.Sector = -1;
            header.Length = length;
        }

        public int GetNext(int sector, bool create)
        {
            var result = next[sector];
            if (result == 0)
            {
                if (create)
                    result = AddSector(sector);
                else throw new Exception();
            }
            return result;
        }

        public int AddSector()
        {
            int result;
            if (available.Count > 0)
            {
                result = available.First();
                available.Remove(result);
            }
            else
            {
                next.Add(0);
                result = next.Count - 1;
                var required = next.Count*SectorSize;
                if (file.Length < required)
                    file.SetLength(required + 100*SectorSize);
            }
            return result;
        }

        public int AddSector(int sector)
        {
            Debug.Assert(this.next[sector] == 0);
            var next = AddSector();
            this.next[sector] = next;
            return next;
        }

        public void RemoveSector(int sector)
        {
            next[sector] = -1;
            available.Add(sector);
        }

        public void CloseStream(bool cut = true)
        {
            _header = null;
        }

        public void Clear()
        {
            if (_header != null) throw new Exception("Stream is active");
            next.Clear();
            available.Clear();
            file.SetLength(0);
        }

        internal void Optimize(IEnumerable<Header> headers)
        {
            Truncate();
            //TODO: Defragmentation
        }
    }
}