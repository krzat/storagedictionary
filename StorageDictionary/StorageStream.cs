﻿using System;
using System.Diagnostics;
using System.IO;

namespace StorageLib
{
    /// <summary>
    /// Streams data to/from the correct sectors
    /// </summary>
    internal class StorageStream : Stream
    {
        private BaseStorage _baseStorage;
        private FileStream file;
        private Header header;


        private int position;
        private int sector;

        public StorageStream(BaseStorage _baseStorage, FileStream file)
        {
            this.file = file;
            this._baseStorage = _baseStorage;
        }


        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override long Length
        {
            get { return header.Length; }
        }

        public override long Position
        {
            get { return position; }
            set { Seek(value, SeekOrigin.Begin); }
        }

        public void Open(Header header)
        {
            this.header = header;
            sector = header.Sector;
            file.Position = sector*_baseStorage.SectorSize;
        }

        public override void Flush()
        {
            SetLength(0);
        }

        private int GetLeftSpace()
        {
            var pos = (int) file.Position - sector*_baseStorage.SectorSize;
            var space = _baseStorage.SectorSize - pos;
            Debug.Assert(space >= 0);
            return space;
        }

        private bool MoveNext(bool canAdd)
        {
            var current = sector;
            sector = _baseStorage.GetNext(sector, canAdd);
            if (sector == current) return false;
            file.Position = sector*_baseStorage.SectorSize;
            return true;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var result = 0;
            while (count > 0)
            {
                var space = GetLeftSpace();
                if (space == 0)
                {
                    MoveNext(false);
                    space = GetLeftSpace();
                }
                var max = Length - Position;
                space = Math.Min(space, (int) max);
                space = Math.Min(space, count);
                if (space == 0) return result;

                result += file.Read(buffer, offset, space);
                //return 7;
                count -= space;
                offset += space;
                position += space;
            }
            return result;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            var pos = (int) offset;
            if (origin == SeekOrigin.Current) pos += position;
            if (origin == SeekOrigin.End) pos += header.Length;

            var sectors = pos/_baseStorage.SectorSize;
            sector = header.Sector;
            for (var i = 0; i < sectors; i++)
                sector = _baseStorage.GetNext(sector, false);
            file.Position = pos%_baseStorage.SectorSize;
            position = pos;
            header.Length = Math.Max(header.Length, position);
            return position;
        }

        public override void SetLength(long value)
        {
            _baseStorage.SetLength(header, (int) value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            while (count > 0)
            {
                var space = GetLeftSpace();
                if (space == 0) MoveNext(true);
                space = Math.Min(space, count);

                file.Write(buffer, offset, space);

                count -= space;
                offset += space;
                position += space;
                header.Length = Math.Max(header.Length, position);
            }
        }

        protected override void Dispose(bool disposing)
        {
            position = 0;
            _baseStorage.CloseStream();
            base.Dispose(disposing);
        }
    }
}