﻿using ProtoBuf;

namespace StorageLib
{
    /// <summary>
    /// Maps entry to the sector
    /// </summary>
    [ProtoContract]
    class Header
    {
        [ProtoMember(1, IsRequired = true)]
        public int Sector = -1;

        [ProtoMember(2)]
        public int Length;

        public override string ToString()
        {
            return Sector + ":" + Length;
        }
    }
}