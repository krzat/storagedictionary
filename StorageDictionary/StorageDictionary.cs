﻿using System;
using System.Collections.Generic;
using System.IO;
using ProtoBuf;
using ProtoBuf.Meta;

namespace StorageLib
{
    /// <summary>
    /// Provides access to the data on hard drive 
    /// </summary>
    /// <typeparam name="K">Key (must have ProtoContract)</typeparam>
    /// <typeparam name="V">Value (must have ProtoContract)</typeparam>
    [ProtoContract]
    public class StorageDictionary<K, V> : IDisposable
    {
        private string headerPath;

        [ProtoMember(1)] private Dictionary<K, Header> headers = new Dictionary<K, Header>();

        [ProtoMember(2)] private BaseStorage storage;

        static StorageDictionary()
        {
            var k = RuntimeTypeModel.Default.CanSerialize(typeof (K));
            if(!k) throw new Exception("Key is not serializable by protobuf-net");

            var v = RuntimeTypeModel.Default.CanSerialize(typeof (V));
            if (!v) throw new Exception("Value is not serializable by protobuf-net");
        }

        public StorageDictionary(string path, int sectorSize = 5000)
        {
            headerPath = path;
            var model = RuntimeTypeModel.Default;
            if (File.Exists(path))
            {
                using (var file = File.OpenRead(path))
                {
                    model.Deserialize(file, this, GetType());
                }
                if (storage.SectorSize != sectorSize) throw new ArgumentException("Wrong storage size");
            }
            storage = new BaseStorage(path + ".data", sectorSize);
            storage.Initialize();
        }

        public V this[K key]
        {
            get { return Get(key); }
            set { Set(key, value); }
        }

        public void Set(K key, V value)
        {
            if (value == null)
            {
                Remove(key);
                return;
            }

            Header header;
            headers.TryGetValue(key, out header);
            if (header == null)
            {
                header = new Header();
                headers.Add(key, header);
            }

            using (var stream = storage.Open(header))
            {
                Serializer.Serialize(stream, value);
                stream.SetLength(stream.Position);
            }
        }

        public V Get(K key)
        {
            Header header;
            headers.TryGetValue(key, out header);
            if (header == null) return default(V);

            using (var stream = storage.Open(header))
            {
                return Serializer.Deserialize<V>(stream);
            }
        }

        public void Remove(K key)
        {
            Header header;
            headers.TryGetValue(key, out header);
            if (header == null) return;

            headers.Remove(key);
            storage.Remove(header);
        }

        public void Save()
        {
            var model = RuntimeTypeModel.Default;
            using (var file = File.Create(headerPath))
            {
                //Serializer.NonGeneric.Serialize(file, this);
                model.Serialize(file, this);
            }
        }

        public void Dispose()
        {
            Save();
            storage.Dispose();
        }

        public int Count
        {
            get { return headers.Count; }
        }
        public IEnumerable<K> Keys
        {
            get { return headers.Keys; }
        }

        public bool ContainsKey(K key)
        {
            return headers.ContainsKey(key);
        }

        public void Clear()
        {
            headers.Clear();
            storage.Clear();
        }

        /// <summary>
        /// Optimizes disk usage and performance.
        /// May take a long time.
        /// </summary>
        public void Optimize()
        {
            storage.Optimize(headers.Values);
        }
    }
}