﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageLib;
using System.IO;

namespace StorageTest
{
    class Program
    {
        static void Assert(bool value, string msg ="")
        {
            if(!value) throw new Exception(msg);
        }

        static void Test1()
        {
            File.Delete("x");

            var storage = new StorageDictionary<int, string>("x", 100);

            storage.Set(0, "hello");
            storage[1] = "hello2";

            Assert("hello" == storage[0]);
            Assert(storage.Count == 2);

            storage.Dispose();

            storage = new StorageDictionary<int, string>("x", 100);

            Assert("hello" == storage[0]);
            Assert(storage.Count == 2);

            storage.Dispose();
        }

        static void Main(string[] args)
        {
            File.Delete("x");
            Test1();
            Test2();
        }

        private static void Test2()
        {
            File.Delete("x");
            var storage = new StorageDictionary<string, byte[]>("x", 100);
            var bytes = new byte[] {1, 2, 3, 4, 5, 6};
            storage["x"] = bytes;
            storage.Dispose();

            storage = new StorageDictionary<string, byte[]>("x", 100);
            bytes = storage["x"];
            Assert(bytes[0] == 1 && bytes[5] ==6);
            storage.Dispose();
        }
    }
}
