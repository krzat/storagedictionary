StorageDictionary
=================

Simple virtual file system with access by key. Uses protobuf-net for serialization (keys and values has to be serializable).
Keys are being kept in memory, but values are loaded on demand.

How it works:

Storage consist of two files: header and data. Header is always loaded into memory. Data file is divided into sectors. Each time entry is added, storage allocates new sector and writes data there. That sector is then assigned to provided key.
If value is bigger than the sector, storage can allocate more sectors and chain them like linked list.


Example:

```csharp
var storage = new StorageDictionary<int, string>("file.bin", 100); 
//load or create new storage in file.bin, 100 bytes per sector

storage[0] = "hello world"; 
//save string on hdd

var str = storage[0]; //load string from hdd

storage.Dispose(); 
//save keys metadata on hdd
```

License: http://www.apache.org/licenses/LICENSE-2.0
